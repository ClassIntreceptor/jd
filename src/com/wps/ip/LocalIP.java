package com.wps.ip;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;

public class LocalIP {
    public static InetAddress inetAddress;
       public static void main(String[] args) throws Exception {
           Enumeration<NetworkInterface> byName1 =NetworkInterface.getNetworkInterfaces();
           while(byName1.hasMoreElements()){
               NetworkInterface networkInterface = byName1.nextElement();
               Enumeration<InetAddress> inetAddresses = networkInterface.getInetAddresses();
                while (inetAddresses.hasMoreElements()){
                    InetAddress inetAddress = inetAddresses.nextElement();
                    String hostAddress = inetAddress.getHostAddress();
                    if (hostAddress.indexOf("192")!=(-1)){
                        System.out.println(hostAddress);
                    }
                }
           }
       }
}
